package com.l.ctest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.l.ctest.R
import com.l.ctest.data.model.Blog
import kotlinx.android.synthetic.main.blog_item.view.*
import java.util.*

class BlogAdapter(
    private val onBlogListener: OnBlogListener
) : RecyclerView.Adapter<BlogAdapter.BlogViewHolder>() {

    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Blog>() {

        override fun areItemsTheSame(oldItem: Blog, newItem: Blog): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Blog, newItem: Blog): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)


    fun setData(list: List<Blog>) {
        differ.submitList(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogViewHolder {
        return BlogViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.blog_item, parent, false),
            onBlogListener
        )
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
        holder.apply {
            title.text = differ.currentList[position].title.substring(0,3).toUpperCase(Locale.ROOT)
            price.text = "$ ${differ.currentList[position].id},000"
        }
    }

    class BlogViewHolder(itemView: View, private val onBlogListener: OnBlogListener) :
        RecyclerView.ViewHolder(itemView) {

        var title: TextView = itemView.tv_title
        var price: TextView = itemView.tv_price

        init {
            itemView.setOnClickListener {
                onBlogListener.onBlogClick(adapterPosition)
            }
        }
    }

    interface OnBlogListener {
        fun onBlogClick(position: Int)
    }
}