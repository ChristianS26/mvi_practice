package com.l.ctest.utils

class Constants {
    companion object {
        const val LANGUAGE_HEADER_KEY = "Accept-Language"
        const val SPANISH_LANGUAGE_CODE = "es"
        const val LANGUAGE_HEADER_SPANISH = "es-MX"
    }
}