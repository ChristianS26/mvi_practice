package com.l.ctest.network

import com.l.ctest.data.model.Blog
import com.l.ctest.network.mappers.NetworkMapper
import com.l.ctest.network.retrofit.ApiServiceImpl
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BlogServiceImpl
@Inject
constructor(
    private val serviceImpl: ApiServiceImpl,
    private val networkMapper: NetworkMapper
) : BlogService {

    override fun getBlogsAsync(callback: IBlogServiceImpl): Disposable {

        return Observable.interval(0, 5, TimeUnit.SECONDS)
            .flatMap { serviceImpl.getBlogs() }
            .subscribeOn(Schedulers.io())
            //.flatMap { response -> Observable.fromIterable(response).timeInterval() }
            //.flatMap { res -> retrofitServiceImpl.getBlogs() }
            .map { networkMapper.mapFromEntityList(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.getResponse(it)
                }, {
                    callback.onError(it)
                }
            )
    }

    override fun getBlogs(callback: IBlogServiceImpl): Disposable {
        return serviceImpl.getBlogs()
            .subscribeOn(Schedulers.io())
            .map { networkMapper.mapFromEntityList(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.getResponse(it)
                }, {
                    callback.onError(it)
                }
            )
    }

    interface IBlogServiceImpl {
        fun getResponse(list: List<Blog>)
        fun onError(error: Throwable)
    }
}