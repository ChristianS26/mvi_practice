package com.l.ctest.network

import io.reactivex.disposables.Disposable

interface BlogService {

    fun getBlogs(callback: BlogServiceImpl.IBlogServiceImpl): Disposable


    fun getBlogsAsync(callback: BlogServiceImpl.IBlogServiceImpl): Disposable
}