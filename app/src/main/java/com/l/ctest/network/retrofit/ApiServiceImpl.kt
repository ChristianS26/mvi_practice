package com.l.ctest.network.retrofit

import com.l.ctest.data.model.Book
import com.l.ctest.data.model.Ticker
import com.l.ctest.network.model.BlogEntity
import io.reactivex.Observable
import javax.inject.Inject

class ApiServiceImpl @Inject constructor(private val apiService: ApiService) :
    ApiService {

    override fun getAvailableBooks(): Observable<Book> {
        return apiService.getAvailableBooks()
    }

    override fun getTicker(): Observable<Ticker> {
        return apiService.getTicker()
    }

    override fun getBlogs(): Observable<List<BlogEntity>> {
        return apiService.getBlogs()
    }
}