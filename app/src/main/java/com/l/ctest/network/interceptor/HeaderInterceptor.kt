package com.l.ctest.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import java.util.*
import javax.inject.Inject

class HeaderInterceptor @Inject constructor() : Interceptor {

    companion object {
        const val LANGUAGE_HEADER_KEY = "Accept-Language"
        const val SPANISH_LANGUAGE_CODE = "es"
        const val LANGUAGE_HEADER_SPANISH = "es-MX"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()

        Locale.getDefault().displayLanguage.let {
            if (it == SPANISH_LANGUAGE_CODE) {
                request.addHeader(LANGUAGE_HEADER_KEY, LANGUAGE_HEADER_SPANISH)
            }
        }

        return chain.proceed(request.build())
    }

}