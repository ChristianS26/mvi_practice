package com.l.ctest.network.mappers

import com.l.ctest.data.model.Blog
import com.l.ctest.network.model.BlogEntity
import com.l.ctest.utils.EntityMapper
import javax.inject.Inject

class NetworkMapper
@Inject
constructor() : EntityMapper<BlogEntity, Blog> {

    override fun mapFromEntity(entity: BlogEntity): Blog {
        return Blog(
            id = entity.id,
            title = entity.title,
            body = entity.body,
            image = entity.image,
            category = entity.category
        )
    }

    override fun mapToEntity(domainModel: Blog): BlogEntity {
        return BlogEntity(
            id = domainModel.id,
            title = domainModel.title,
            body = domainModel.body,
            image = domainModel.image,
            category = domainModel.category
        )
    }

    fun mapFromEntityList(entities: List<BlogEntity>): List<Blog> {
        return entities.map { mapFromEntity(it) }
    }

}