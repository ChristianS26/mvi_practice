package com.l.ctest.network.retrofit


import com.l.ctest.data.model.Blog
import com.l.ctest.data.model.Book
import com.l.ctest.data.model.Ticker
import com.l.ctest.network.model.BlogEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @POST("/v3/available_books/")
    fun getAvailableBooks(): Observable<Book>

    @POST("/v3/ticker/")
    fun getTicker(): Observable<Ticker>

    @GET("blogs")
    fun getBlogs(): Observable<List<BlogEntity>>


    //@GET("blog/oliver-random-blog/")
    //fun getRandomBlog() Single
}