package com.l.ctest

import com.l.ctest.data.model.Book
import com.l.ctest.data.model.OrderLimits

object DataSource {

    fun getBooks(): Book {
        return Book(
            success = true,
            orderLimits = arrayListOf(
                OrderLimits(
                    book = "btc_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "eth_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "xpr_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "ltc_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "bch_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "tusd_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                )
                , OrderLimits(
                    book = "mana_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "btc_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "eth_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "xpr_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "ltc_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "bch_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                ),
                OrderLimits(
                    book = "tusd_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                )
                , OrderLimits(
                    book = "mana_mxn",
                    minimum_amount = ".003",
                    maximum_amount = "1000.00",
                    minimum_price = "100.00",
                    maximum_price = "1000000.00",
                    minimum_value = "25.00",
                    maximum_value = "1000000.00"
                )
            )
        )
    }
}