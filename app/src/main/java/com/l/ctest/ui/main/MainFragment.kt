package com.l.ctest.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.l.ctest.R
import com.l.ctest.adapters.BlogAdapter
import com.l.ctest.data.model.Blog
import com.l.ctest.data.state.DataState
import com.l.ctest.ui.blogdetail.BlogDetailFragment
import com.l.ctest.utils.ItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.main_fragment.*


@AndroidEntryPoint
class MainFragment : Fragment(R.layout.main_fragment), BlogAdapter.OnBlogListener {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var blogAdapter: BlogAdapter
    private var blogs = listOf<Blog>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setStateEvent(MainStateEvent.GetBlogsEvent)
        viewModel.dataState.observe(viewLifecycleOwner, Observer(::render))

        blogAdapter = BlogAdapter(this)
        initListeners()
        initRecyclerView()
    }

    private fun render(dataState: DataState<List<Blog>>) {
        ly_swipe.isRefreshing = DataState.Loading == dataState
        when (dataState) {
            is DataState.Success<List<Blog>> -> {
                blogs = dataState.data
                blogAdapter.setData(blogs)
                ly_retry.visibility = View.GONE
                ly_swipe.visibility = View.VISIBLE
            }
            is DataState.Error -> {
                ly_retry.visibility = View.VISIBLE
                ly_swipe.visibility = View.GONE
            }
        }
    }

    private fun initRecyclerView() {
        rv_blogs.adapter = blogAdapter
        rv_blogs.addItemDecoration(ItemDecoration(requireContext(), R.dimen.item_offset))
    }

    private fun initListeners() {

        ly_swipe.setOnRefreshListener { viewModel.setStateEvent(MainStateEvent.RefreshBlogsEvent) }

        btn_retry.setOnClickListener {
            viewModel.setStateEvent(MainStateEvent.RefreshBlogsEvent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }

    override fun onBlogClick(position: Int) {
        if (childFragmentManager.findFragmentByTag("book_detail") == null)
            BlogDetailFragment(blogs[position].title).show(
                childFragmentManager.beginTransaction(),
                "book_detail"
            )
        /* activity?.supportFragmentManager?.beginTransaction()!!
             .setCustomAnimations(R.anim.slide_up, 0, 0, R.anim.slide_bottom)
             .add(R.id.container, FragmentTest())
             .addToBackStack(null)
             .commit()*/
    }

}