package com.l.ctest.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.l.ctest.data.model.Blog
import com.l.ctest.data.state.DataState
import com.l.ctest.network.BlogServiceImpl
import io.reactivex.disposables.CompositeDisposable

class MainViewModel
@ViewModelInject
constructor(
    private val blogServiceImpl: BlogServiceImpl
) :
    ViewModel(), BlogServiceImpl.IBlogServiceImpl {
    private val _dataState: MutableLiveData<DataState<List<Blog>>> = MutableLiveData()
    private val _disposable = CompositeDisposable()

    val dataState: LiveData<DataState<List<Blog>>>
        get() = _dataState

    fun setStateEvent(mainStateEvent: MainStateEvent) {

        when (mainStateEvent) {
            is MainStateEvent.GetBlogsEvent -> {
                _dataState.postValue(DataState.Loading)
                //_disposable.add(blogServiceImpl.getBlogs(this))
                _disposable.add(blogServiceImpl.getBlogs(this))
            }
            is MainStateEvent.RefreshBlogsEvent -> {
                _dataState.postValue(DataState.Loading)
               // _disposable.add(blogServiceImpl.getBlogs(this))
                _disposable.add(blogServiceImpl.getBlogs(this))
            }
        }
    }

    override fun getResponse(list: List<Blog>) {
        _dataState.postValue(DataState.Success(list))
    }

    override fun onError(error: Throwable) {
        _dataState.postValue(DataState.Error(error))
    }

    fun dispose() {
        _disposable.clear()
    }
}

sealed class MainStateEvent {
    object GetBlogsEvent : MainStateEvent()
    object RefreshBlogsEvent : MainStateEvent()
}