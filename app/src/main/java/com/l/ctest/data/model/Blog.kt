package com.l.ctest.data.model

data class Blog(
    val body: String,
    val category: String,
    val image: String,
    val id: Int,
    val title: String
)