package com.l.ctest.data.model

import com.google.gson.annotations.SerializedName

data class Ticker(
    @SerializedName("payload")
    val tickerInformation: TickerInformation,
    @SerializedName("success")
    val success: Boolean
)