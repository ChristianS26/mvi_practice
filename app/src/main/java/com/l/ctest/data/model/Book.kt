package com.l.ctest.data.model


data class Book(
    val orderLimits: List<OrderLimits>,
    val success: Boolean
)